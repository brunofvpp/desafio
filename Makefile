build:
	docker build -t desafio_orama .

run:
	docker run -d -p 8000:8000 --name desafio_orama desafio_orama

stop:
	docker stop desafio_orama
	docker rm desafio_orama

test:
	docker exec -it desafio_orama pytest
