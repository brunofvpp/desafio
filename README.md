# Desafio: Bruno Fagundes (bruno.fvpp@gmail.com)
O desafio foi feito usando [Django](https://www.djangoproject.com/), [DRF](https://www.django-rest-framework.org/) para construção da api e [Pytest](https://docs.pytest.org/en/stable/) para os testes. Para manipular as datas utilizei o metodo `fromordinal` presente apartir da versão 3.7 do python, escolha justificada pela melhor elegibilidade.

## Running with docker

#### Requeriments
- [Docker](https://docs.docker.com/get-docker/)

#### How to use

`make build`: Build app image;

`make run`: Create app container, start development server at [http://0.0.0.0:8000/api/freelancer](http://0.0.0.0:8000/api/freelancer);

`make stop`: Stop and remove app container;

`make test`: Run tests, need container running (`make run`);

## Running without docker

#### Requeriments
- [Python 3.7+](https://www.python.org/downloads/)

#### How to use
`pip install -r requirements.txt`: Install dependencies;

`python manage.py migrate`: Running database migrations;

`python manage.py runserver`: Start development server at [http://0.0.0.0:8000/api/freelancer](http://0.0.0.0:8000/api/freelancer);

`pytest`: Run tests;
