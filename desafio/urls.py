from django.urls import include, path
from django.views.generic import RedirectView
from rest_framework import permissions


urlpatterns = [
    path('', RedirectView.as_view(pattern_name='core:freelancer_api')),
    path('api/freelancer', include('desafio.core.urls'))
]
