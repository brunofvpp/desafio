import pytest
from desafio.core.classes import Freelancer


def test_freelancer_calculate_skill_duration_in_months_without_overlapping():
    data = {
      "freelance": {
        "id": 42,
        "user": {
          "firstName": "Hunter",
          "lastName": "Moore",
          "jobTitle": "Fullstack JS Developer"
        },
        "status": "new",
        "retribution": 650,
        "availabilityDate": "2018-06-13T00:00:00+01:00",
        "professionalExperiences": [
          {
            "id": 4,
            "companyName": "Okuneva, Kerluke and Strosin",
            "startDate": "2016-01-01T00:00:00+01:00",
            "endDate": "2016-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2017-01-01T00:00:00+01:00",
            "endDate": "2017-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2018-01-01T00:00:00+01:00",
            "endDate": "2018-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              },
              {
                "id": 370,
                "name": "Javascript"
              }
            ]
          }
        ]
      }
    }
    freelancer = Freelancer(data=data)
    result = freelancer.calculate_skill_duration_in_months()
    freelance = result["freelance"]

    assert freelance["id"]  == 42

    assert freelance["computedSkills"][0]["id"] == 241
    assert freelance["computedSkills"][0]["name"] == "React"
    assert freelance["computedSkills"][0]["durationInMonths"] == 33

    assert freelance["computedSkills"][1]["id"] == 270
    assert freelance["computedSkills"][1]["name"] == "Node.js"
    assert freelance["computedSkills"][1]["durationInMonths"] == 22

    assert freelance["computedSkills"][2]["id"] == 370
    assert freelance["computedSkills"][2]["name"] == "Javascript"
    assert freelance["computedSkills"][2]["durationInMonths"] == 11


def test_freelancer_calculate_skill_duration_in_months_with_overlapping():
    data = {
      "freelance": {
        "id": 42,
        "user": {
          "firstName": "Hunter",
          "lastName": "Moore",
          "jobTitle": "Fullstack JS Developer"
        },
        "status": "new",
        "retribution": 650,
        "availabilityDate": "2018-06-13T00:00:00+01:00",
        "professionalExperiences": [
          {
            "id": 4,
            "companyName": "Okuneva, Kerluke and Strosin",
            "startDate": "2016-01-01T00:00:00+01:00",
            "endDate": "2016-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2016-08-01T00:00:00+01:00",
            "endDate": "2017-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2018-01-01T00:00:00+01:00",
            "endDate": "2018-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              },
              {
                "id": 370,
                "name": "Javascript"
              }
            ]
          }
        ]
      }
    }
    freelancer = Freelancer(data=data)
    result = freelancer.calculate_skill_duration_in_months()
    freelance = result["freelance"]

    assert freelance["id"]  == 42

    assert freelance["computedSkills"][0]["id"] == 241
    assert freelance["computedSkills"][0]["name"] == "React"
    assert freelance["computedSkills"][0]["durationInMonths"] == 34

    assert freelance["computedSkills"][1]["id"] == 270
    assert freelance["computedSkills"][1]["name"] == "Node.js"
    assert freelance["computedSkills"][1]["durationInMonths"] == 34

    assert freelance["computedSkills"][2]["id"] == 370
    assert freelance["computedSkills"][2]["name"] == "Javascript"
    assert freelance["computedSkills"][2]["durationInMonths"] == 11


def test_freelancer_calculate_skill_duration_in_months_with_duplicated_dates():
    data = {
      "freelance": {
        "id": 42,
        "user": {
          "firstName": "Hunter",
          "lastName": "Moore",
          "jobTitle": "Fullstack JS Developer"
        },
        "status": "new",
        "retribution": 650,
        "availabilityDate": "2018-06-13T00:00:00+01:00",
        "professionalExperiences": [
          {
            "id": 4,
            "companyName": "Okuneva, Kerluke and Strosin",
            "startDate": "2016-01-01T00:00:00+01:00",
            "endDate": "2016-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2016-08-01T00:00:00+01:00",
            "endDate": "2017-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2016-01-01T00:00:00+01:00",
            "endDate": "2016-12-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              },
              {
                "id": 370,
                "name": "Javascript"
              }
            ]
          }
        ]
      }
    }
    freelancer = Freelancer(data=data)
    result = freelancer.calculate_skill_duration_in_months()
    freelance = result["freelance"]

    assert freelance["id"]  == 42

    assert freelance["computedSkills"][0]["id"] == 241
    assert freelance["computedSkills"][0]["name"] == "React"
    assert freelance["computedSkills"][0]["durationInMonths"] == 23

    assert freelance["computedSkills"][1]["id"] == 270
    assert freelance["computedSkills"][1]["name"] == "Node.js"
    assert freelance["computedSkills"][1]["durationInMonths"] == 23

    assert freelance["computedSkills"][2]["id"] == 370
    assert freelance["computedSkills"][2]["name"] == "Javascript"
    assert freelance["computedSkills"][2]["durationInMonths"] == 11
