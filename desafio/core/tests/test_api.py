import pytest
from django.urls import reverse


def test_api_with_invalid_payload_should_fail(client):
    path = reverse('core:freelancer_api')
    data = {}
    response = client.post(path=path, data=data, content_type="application/json")
    assert response.status_code == 422
    assert response.data == None


def test_api_with_valid_payload_should_succeed(client):
    path = reverse('core:freelancer_api')
    data = {
      "freelance": {
        "id": 42,
        "user": {
          "firstName": "Hunter",
          "lastName": "Moore",
          "jobTitle": "Fullstack JS Developer"
        },
        "status": "new",
        "retribution": 650,
        "availabilityDate": "2018-06-13T00:00:00+01:00",
        "professionalExperiences": [
          {
            "id": 4,
            "companyName": "Okuneva, Kerluke and Strosin",
            "startDate": "2016-01-01T00:00:00+01:00",
            "endDate": "2018-05-01T00:00:00+01:00",
            "skills": [
              {
                "id": 241,
                "name": "React"
              },
              {
                "id": 270,
                "name": "Node.js"
              },
              {
                "id": 370,
                "name": "Javascript"
              }
            ]
          },
          {
            "id": 54,
            "companyName": "Hayes - Veum",
            "startDate": "2014-01-01T00:00:00+01:00",
            "endDate": "2016-09-01T00:00:00+01:00",
            "skills": [
              {
                "id": 470,
                "name": "MySQL"
              },
              {
                "id": 400,
                "name": "Java"
              },
              {
                "id": 370,
                "name": "Javascript"
              }
            ]
          },
          {
            "id": 80,
            "companyName": "Harber, Kirlin and Thompson",
            "startDate": "2013-05-01T00:00:00+01:00",
            "endDate": "2014-07-01T00:00:00+01:00",
            "skills": [
              {
                "id": 370,
                "name": "Javascript"
              },
              {
                "id": 400,
                "name": "Java"
              }
            ]
          }
        ]
      }
    }
    response = client.post(path=path, data=data, content_type="application/json")
    assert response.status_code == 200
    assert response.data["freelance"]["id"] == 42

    computed_skills = response.data["freelance"]["computedSkills"]

    assert computed_skills[0]["id"] == 241
    assert computed_skills[0]["name"] == "React"
    assert computed_skills[0]["durationInMonths"] == 28

    assert computed_skills[1]["id"] == 270
    assert computed_skills[1]["name"] == "Node.js"
    assert computed_skills[1]["durationInMonths"] == 28

    assert computed_skills[2]["id"] == 370
    assert computed_skills[2]["name"] == "Javascript"
    assert computed_skills[2]["durationInMonths"] == 60

    assert computed_skills[3]["id"] == 470
    assert computed_skills[3]["name"] == "MySQL"
    assert computed_skills[3]["durationInMonths"] == 32

    assert computed_skills[4]["id"] == 400
    assert computed_skills[4]["name"] == "Java"
    assert computed_skills[4]["durationInMonths"] == 40
