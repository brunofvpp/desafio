from datetime import datetime


class Skill:

    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.days = set()


class Freelancer:

    def __init__(self, data):
        self.id = data["freelance"]["id"]
        self.professional_experiences = data["freelance"]["professionalExperiences"]

    def calculate_skill_duration_in_months(self):
        MONTH_LENGTH = 30
        skills = {}
        computed_skills = []

        for professional_experience in self.professional_experiences:
            start_date = datetime.fromisoformat(professional_experience["startDate"])
            end_date = datetime.fromisoformat(professional_experience["endDate"])

            for skill in professional_experience["skills"]:
                skill_id = skill["id"]
                skill_name = skill["name"]

                # get or create skill object
                try:
                    skill = skills[skill_id]
                except KeyError:
                    skill = Skill(id=skill_id, name=skill_name)
                    skills.update({skill_id: skill})

                unique_days_between_dates = set(
                    datetime.fromordinal(date) for date in range(start_date.toordinal(), end_date.toordinal())
                )
                skill.days.update(unique_days_between_dates)

        for computed_skill in skills:
            duration_in_months = len(skills[computed_skill].days) // MONTH_LENGTH
            computed_skills.append({
                "id": skills[computed_skill].id,
                "name": skills[computed_skill].name,
                "durationInMonths": duration_in_months
            })

        data = {
            "freelance": {
                "id": self.id,
                "computedSkills": computed_skills
            }
        }

        return data
