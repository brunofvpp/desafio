from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_422_UNPROCESSABLE_ENTITY
from .classes import Freelancer


@api_view(['POST'])
def freelancer(request):
    try:
        data = request.data
        freelancer = Freelancer(data=data)
        data = freelancer.calculate_skill_duration_in_months()
        return Response(data, status=HTTP_200_OK)
    except Exception:
        return Response(status=HTTP_422_UNPROCESSABLE_ENTITY)
