FROM python:3.8-slim

ENV PYTHONUNBUFFERED 1

COPY requirements.txt /

RUN pip install -r requirements.txt

WORKDIR /app

COPY . /app

RUN python manage.py migrate

EXPOSE 8000/tcp

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
